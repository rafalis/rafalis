```
  _____            ______      _      _____  _____ 
 |  __ \     /\   |  ____/\   | |    |_   _|/ ____|
 | |__) |   /  \  | |__ /  \  | |      | | | (___  
 |  _  /   / /\ \ |  __/ /\ \ | |      | |  \___ \ 
 | | \ \  / ____ \| | / ____ \| |____ _| |_ ____) |
 |_|  \_\/_/    \_\_|/_/    \_\______|_____|_____/ 
                                                   
```
# Rafalis - Online Image Portfolio

For an example of usage see [the visuals section below](#visuals)

## Description

### Overview

Rafalis is an application designed to display, manage, and share images. Principal features :
 - Display images with context (camera, author, description, location...)
 - Create collections (image regroupments with possibility to write long texts)
 - Locate images on interactive maps
 - Login system, allowing creators to restrict access to a group of user (private VS public pictures)
 - Manage everything from the web application (creator page).
 - Mobile support

## Requirements
 - Client specific :
    - `node & npm`
 - Server specific :
    - `Java (JDK 11)`
    - `openssl` to generate rsa keys and permit authentication
 - (optional, for quickstart) `docker` and `docker-compose`


## Usage

### Clone this repository

Make sure to include the 3 submodules.

```bash
$ git clone --recurse-submodules git@gitlab.com:rafalis/rafalis.git
```

### Quickstart

1. This will launch an empty version of the application :

    ```bash
    docker-compose --env-file sample_env up -d
    ```

2. *If* that is the first initialization of the database, run the following (one-time run script) :

    ```bash
    ./rafalis-db/post-start-install.sh
    ```

3. Done ! The client is accessible at http://localhost:4200. The server's API is available at http://localhost:8080/.


### Development

For dev mode, you can launch separately the database, the server and the client :

- Database :
    ```bash
    $ cd rafalis-db
    $ docker-compose up -d
    ```

- Server :

    Assuming `JAVA_HOME` is configured correctly (points to `JDK 11`, ex: `JAVA_HOME=/home/{user}/.jdks/openjdk-11`), and you have generated a pair of rsa keys (see server's README for more informations)
    ```bash
    $ cd rafalis-server
    $ ./mvnw compile quarkus:dev
    ```

- Client :
    ```bash
    $ cd rafalis-client
    $ npm ci
    $ ng serve
    ```

The application is now accessible `http://localhost:4200/`.

The server api is listening at `http://localhost:8080/`, with a gui at `http://localhost:8080/q/swagger-ui/`

---

## Visuals
Some examples of the application in use.

### Viewer
<figcaption align = "center">Image Viewer</figcaption>

![Image viewer](img/viewer-1.png "Image viewer")

<figcaption align = "center">Image description editor</figcaption>

![Image editor](img/viewer-edit.png "Image editor")

<figcaption align = "center">Image Viewer - placed on map</figcaption>

![Image viewer](img/viewer-2.png "Image viewer")

<figcaption align = "center">Image Viewer - dynamic background</figcaption>

![Image viewer](img/viewer-3.png "Image viewer")

<figcaption align = "center">Image upload</figcaption>

![Image adder](img/img-adder.png "Image adder")


### Home page

<figcaption align = "center">Home page</figcaption>

![Home page](img/home.png "Home page")


### Browser

<figcaption align = "center">Browse page</figcaption>

![Browse page](img/browse.png "Browse page")


<figcaption align = "center">Browse page - text filter</figcaption>

![Browse page](img/filters-1.png "Browse page")


<figcaption align = "center">Browse page - filter by color</figcaption>

![Browse page](img/filters-color.png "Browse page")

### Collections

<figcaption align = "center">Collections - menu</figcaption>

![Collections](img/collection-menu.png "Collections")


<figcaption align = "center">Collections - viewer</figcaption>

![Collections](img/collection-viewer.png "Collections")


<figcaption align = "center">Collections edition</figcaption>

![Collections edition](img/collection-editor.png "Collections 
Edition")

### Maps

<figcaption align = "center">Maps</figcaption>

![Maps](img/maps.png "Maps")

<figcaption align = "center">Maps - editor</figcaption>

![Maps](img/map-editor.png "Maps")


#### Administration

<figcaption align = "center">Creator panel</figcaption>

![Creator panel](img/admin-1.png "Creator panel")


---

## Wiki

### Authentication and privilege levels
This application provides 3 levels of privileges, which respect the access matrix below :

| Level               |                |                 |               |
|-------------------- | :------------: | :-------------: | :-----------: |
|                     | Public content | Private content | Creator features |
| Anonymous           |       x        |                 |               |
| Logged in as viewer |       x        |        x        |               |
| Logged in as creator|       x        |        x        |      x        |

Content refers to images and collections.
Creator features refer to : modifying images, uploading images, creating and modifying collections, and access to the creator panel.

 > **_NOTE:_** The application will warn you, but if you try to add private images in a public collection, anonymous users will see the private image not loading because they don't have access to it but can still see the collection.


### Adding an image

Upload images and associate them various informations :

| Field             |                                                    |
| ----------------- | -------------------------------------------------- |
| **Title**         | image main title                                   |
| **Author(s)**     | author(s), configurable from the creator panel     |
| **Date**          | date of image (optional)                           |
| **Camera**        | camera, configurable from the creator panel        |
| **Description**   | here you can write some context about the image    |
| **Tags**          | to help you to search your image in the database   |

Moreover, you have 3 radio toggles :

| Toggle            |                                                    |
| ----------------- | -------------------------------------------------- |
| **Hide in browse**| **When *ON*, the image will not be shown in the browse page, but you will still be able to add it in a collection**. I typically use this to add travel pictures, which are not great from a photograph point of view, but which are very nice to have in a collection where the story telling is based on this image.  |
| **Show in homepage** | When *ON*, the image can be randomly selected to be displayed in the home page of the application. |
| **Public** | Whether the image is private or public. See [this section](#authentication-and-privilege-levels) for explanation on the private/public notion.|

### Creating a collection
Once you populated the database with some images, you can create a collection.

From the creator panel, use the `Add collection` button and fill the required fields. Select a thumbnail that will be displayed in the collection page.

Once the collection opened, you can add/modify/remove sections. Three section types are available :
   - **Text** : a text section that will get you a basic text editor.
   - **Single Picture** : a single image. When adding a single picture section, you can choose to automatically add a text section below it containing the center image title.
   - **Carousel** : a carousel containing several images that can be scrolled

Collections can be public or private, just like images.

### Adding a map
For a map, refer to the guide inside the application, available in the creator panel : `(advanced) How do I use maps ?`

## Support
If you have any questions or improvements, feel free to send me an email mathu.chritin@gmail.com, I will be happy to help you or if you have new ideas to present !

## Roadmap
These features are in the sandbox :
 - Implement kml to geojson converter in the server, so that we can import directly the file from Google My Maps in the application and parse it.


# License

TODO
